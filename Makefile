.PHONY: build

SHELL:=/bin/zsh
CHECKING_DIR=app
PATH := $(PATH):$(PWD):$(TEST_FOLDERS_PATH)
GIT_BRANCH = $$(git symbolic-ref --short HEAD)

.EXPORT_ALL_VARIABLES:


.DEFAULT: help

help:
	@echo "\n \
	------------------------------------------------------------ \n \
	\n \
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n \
	++ Python Related ++ \n \
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n \
	- python-poetry-install: Install python and poetry \n \
    - black: Runs a linter (Black) over the whole repo. \n \
    - mypy: Runs a type-checker in the extract dir. \n \
    - pylint: Runs the pylint checker over the whole repo. Does not check for code formatting, only errors/warnings. \n \
    - flake8: Run flake8 library to check Python code \n \
    - vulture: Run vulture library to check unused code \n \
    - pytest: run all pytest cases \n \
    - python-code-quality: integrate one command to run ALL python check (from the previous commands) \n \
    - clean-python: clean up python code and delete cache directories/files \n \
	\n \
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n \
	++ Poetry Related ++ \n \
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n \
    - prepare-poetry: update ad build packages. \n \
	\n \
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n \
	++ Sharehouse Related ++ \n \
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n \
    - run-sharehouse: Running the app. \n \
    - run: Ready to full execution of the app. \n \
	------------------------------------------------------------"

########################################################################################################################
# Python
########################################################################################################################

python-poetry-install:
	@echo "Running python-poetry-install..."
	@bash -c " /bin/zsh install/python_poetry.sh"

black:
	@echo "Running lint (black)..."
	@poetry run black "$(CHECKING_DIR)"

flake8:
	@echo "Running flake8..."
	@poetry run flake8 "$(CHECKING_DIR)" --ignore=E203,E501,W503,W605

mypy:
	@echo "Running mypy..."
	@poetry run mypy "$(CHECKING_DIR)" --ignore-missing-imports

pylint:
	@echo "Running pylint..."
	@poetry run pylint "$(CHECKING_DIR)" --disable=line-too-long,E0401,E0611,W1203,W1202

vulture:
	@echo "Running vulture..."
	@poetry run vulture "$(CHECKING_DIR)" --min-confidence 100

pytest:
	@echo "Running pytest..."
	@poetry run python3 -m pytest -vv -x

python-code-quality: black flake8 vulture pytest pylint mypy
	@echo "Running python-code-quality..."

clean-python:
	@echo "Running clean-python..."
	@poetry env remove python

########################################################################################################################
# Poetry
########################################################################################################################

prepare-poetry:
	@echo "Running prepare-poetry..."
	@poetry install
	@poetry update
	@poetry build

########################################################################################################################
# Sharehouse
########################################################################################################################
run-sharehouse:
	@echo "Running run-sharehouse..."
	@poetry run start

run: prepare-poetry run-sharehouse
	@echo "Running run..."