# Sharehouse

This small project is nucleus of idea how to communicate with internal and external teams automatically.
Using this way of exposing data via `RESTful API `are great opportunity to leverage the `Data` team visibility and faster data delivery in a versatile way.

Also, provide an easy way for:
* data `encapsulation`
* vertical and horizontal `scalability`
* `maintainability` 
* `detaching` computation from storage 
* fine `access` granulation


## Development

### Installation


```bash
# Install pyton and poetry
make python-poetry-install

# Install and update all needed packages to make app work smoothly
make prepare-poetry
```


### Secrets

These secrets should be set in order to make everything works smoothly. You should use `.env` file or expose them as environment variables.
```bash
# Snowflake connection parameters
SF_AUTHENTICATOR={snowflake_authenticator} # [oauth, token, externalbrowser]
SF_USERNAME={snowflake_username}
SF_PASSWORD={snowflake_password} # if SF_AUTHENTICATOR=externalbrowser, leave it blank
SF_ROLE={snowflake_role}        
SF_ACCOUNT={snowflake_account}
SF_WAREHOUSE={snowflake_warehouse}

# App security parameters
ACCESS_TOKEN={sharehouse_access_token}
```

### Run server (dev) locally
```bash
# cd sharehouse/app/sharehouse

uvicorn main:app --reload
```

### Hello world test
```bash
curl -X 'GET'  'http://127.0.0.1:8000/' -H 'accept: application/json'

# You will get: {"message":"Hello GitLabSharehouse!"}
```

### Documentation

When container is up and running, access documentation via this URL:
```bash
http://127.0.0.1:8000/docs

# or, if you like `redoc`:
# http://127.0.0.1:8000/redoc
``` 

## Code quality
```bash
make python-code-quality
```

## Testing

```bash
make pytest
```

## Help

```bash
make help
# or just make
```

## Usage

### Pre-requirements

1. In order to be able to use the application, you should have one of those 2 login options:
    * ❄️`Snowflake` SSO account (accessible via OKTA)
    * ❄️`Snowflake` username/password access

    Without noted settings, the application shouldn't work.
    > **Note:** the way you should input your credentials is described in the next section.
2. You should have at least read access to `gitlab-data/service-ping-metrics-check` project.

### Secrets

These secrets should be set in order to make everything works smoothly. You should use `.env` file or expose them as environment variables.
```bash
# Snowflake connection parameters
SF_AUTHENTICATOR={snowflake_authenticator} # [oauth, token, externalbrowser]
SF_USERNAME={snowflake_username}
SF_PASSWORD={snowflake_password} # if SF_AUTHENTICATOR=externalbrowser, leave it blank
SF_ROLE={snowflake_role}        
SF_ACCOUNT={snowflake_account}
SF_WAREHOUSE={snowflake_warehouse}

# App security parameters
ACCESS_TOKEN={sharehouse_access_token}
```

### Run the app

Download the code:
```bash
git clone git@gitlab.com:gitlab-data/service-ping-metrics-check.git
```

```bash
# Note: set up secrets before you run the app
make run

# You should see something like the following logs
# INFO:     Will watch for changes in these directories: ['.../service-ping-metrics-check']
# INFO:     Uvicorn running on http://0.0.0.0:8000 (Press CTRL+C to quit)
# INFO:     Started reloader process [*****] using StatReload
# INFO:     Started server process [*****]
# INFO:     Waiting for application startup.
# INFO:     Application startup complete.

```


### Documentation
When container is up and running, access documentation via this URL:
```bash
http://127.0.0.1:8000/docs

# or, if you like `redoc`:
# http://127.0.0.1:8000/redoc
``` 

### Hello world test

```bash
curl -X 'GET'  'http://127.0.0.1:8000/' -H 'accept: application/json'
  
# You will get {"message":"Hello GitLabSharehouse!"}
```

### Check metrics code

* `curl`
```bash
curl -X 'POST' \
  'http://127.0.0.1:8000/metrics/execute_snowflake' \
  -H 'accept: application/json' \
  -H 'bearer: {BEARER_TOKEN}' \
  -H 'Content-Type: application/json' \
  -d '{
  "metrics_name": "test_metric",
  "postgres_query": "{POSTGRES_QUERY}"
}'
```

#### Example

Run the following code: 
```bash
curl -X 'POST' \
  'http://127.0.0.1:8000/metrics/execute_snowflake' \
  -H 'accept: application/json' \
  -H 'bearer: {bearer}' \
  -H 'Content-Type: application/json' \
  -d '{
  "metrics_name": "test_metric",
  "postgres_query": "SELECT COUNT(DISTINCT merge_requests.author_id) FROM merge_requests"
}'
```

and you should expect the following response:
```json
{
    "input": {
        "metrics_name": "test_metric",
        "postgres_query": "SELECT COUNT(DISTINCT merge_requests.author_id) FROM merge_requests"
    },
    "output": {
        "status": "OK",
        "snowflake_query": "SELECT 'test_metric' AS counter_name,  COUNT(DISTINCT merge_requests.author_id) AS counter_value, TO_DATE(CURRENT_DATE) AS run_day   FROM prep.gitlab_dotcom.gitlab_dotcom_merge_requests_dedupe_source AS merge_requests",
        "error_message": null,
        "timestamp": "2022-09-27T08:23:24.652497"
    }
}
```

* 🐍Python

```python
import requests

url = "http://127.0.0.1:8000/metrics/execute_snowflake"

payload = "{\n  \"metrics_name\": \"test_metric\",\n  \"postgres_query\": \"{POSTGRES_QUERY}\"\n}"
headers = {
  'Authorization': 'Bearer {BEARER_TOKEN}',
  'Content-Type': 'text/plain'
}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)

```

* Ruby
```bash
require "uri"
require "net/http"

url = URI("http://127.0.0.1:8000/metrics/execute_snowflake")

http = Net::HTTP.new(url.host, url.port);
request = Net::HTTP::Get.new(url)
request["Authorization"] = "Bearer {BEARER_TOKEN}"
request["Content-Type"] = "text/plain"
request.body = "{\n  \"metrics_name\": \"test_metric\",\n  \"postgres_query\": \"{POSTGRES_QUERY}\"\n}"

response = http.request(request)
puts response.read_body
```


