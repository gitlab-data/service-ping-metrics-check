"""
Handle Snowflake connections and query execution
"""
from typing import Tuple, Any

import snowflake.connector
from snowflake.connector import ProgrammingError

import authentication


def snowflake_connect_cursor(config: dict):
    """
    Connect to Snowflake using config
    """
    return snowflake.connector.connect(**config).cursor()


def snowflake_fetch_one(cursor, query: str) -> str:
    """
    Return one row from Snowflake
    """
    cursor.execute(query)

    return cursor.fetchone()


def check_sql(query: str) -> Tuple[Any, Any]:
    """
    Check is SQL on Snowflake executed properly or not
    :param query: str
    :return: Tuple[Any, Any]
    """
    try:
        config = authentication.get_config()

        with snowflake_connect_cursor(config=config) as cur:
            _ = snowflake_fetch_one(cursor=cur, query=query)

            return "OK", None

    except ProgrammingError as programming_error:
        return "FAILED", str(programming_error)
