"""
Entry point for API application
"""
from typing import Dict, Any
from fastapi import FastAPI


import uvicorn
import metrics

APP_TITLE = "GitLabSharehouse"
app = FastAPI(title=APP_TITLE)

app.include_router(metrics.router)


@app.get("/")
async def main() -> Dict[Any, Any]:
    """Test API"""
    hello_message = {"message": f"Hello {APP_TITLE}!"}
    return hello_message


def start():
    """
    Launched with `poetry run start` at root level"
    """
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)
