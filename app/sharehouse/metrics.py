"""
The main module to handle metrics
"""
from typing import Optional, Dict, Any
import datetime

from fastapi import APIRouter
from fastapi import Query, HTTPException, Header
from pydantic import BaseModel

from transform_postgres_snowflake import (
    transform,
)


from snowflake_connect import check_sql
import authentication

router = APIRouter(
    prefix="/metrics",
    tags=["sharehouse"],
    responses={404: {"description": "Not found"}},
)


mandatory_commands = ["SELECT", " "]

forbidden_commands = [
    "INSERT ",
    "UPDATE ",
    "DELETE ",
    "SET ",
    "SHOW ",
    "ALTER ",
    "DROP ",
]


class InputQuery(BaseModel):
    """
    Class to handle properties and inputs for postgres query
    """

    metrics_name: Optional[str] = Query(
        default="test_metric",
        description="Name of the metric",
        min_length=4,
        max_length=200,
    )
    postgres_query: str = Query(
        default="SELECT 1",
        description="Put your Postgres like SQL query here "
        f"The statement can't contain commands: "
        f"{','.join(forbidden_commands)} "
        f"due to security reasons",
        min_length=8,
        max_length=4096,
    )

    def check_forbidden_commands(self) -> None:
        """
        Check is any malicious keyword is passed
        :return: None
        """
        for forbidden_command in forbidden_commands:
            if forbidden_command in self.postgres_query:
                raise HTTPException(
                    status_code=403,
                    detail=f"It is not allowed to have "
                    f"{forbidden_command} in the statement. "
                    f"Allowed command(s) "
                    f"are: {','.join(mandatory_commands)}",
                )

    def check_mandatory_commands(self) -> None:
        """
        See ara all mandatory staffs are inside the SQL
        :return: None
        """
        for mandatory_command in mandatory_commands:
            if mandatory_command in self.postgres_query:
                continue
            raise HTTPException(
                status_code=403,
                detail=f"It is mandatory "
                f"to have following "
                f"commands in the "
                f"statement: {mandatory_command}",
            )


def do_checks(input_query: InputQuery, bearer: str) -> None:
    """
    Apply check for a request
    """
    authentication.check_token(bearer)

    input_query.check_forbidden_commands()

    input_query.check_mandatory_commands()


def generate_result(input_query: InputQuery, snowflake_query: str) -> dict:
    """
    Generate result as a dict
    """
    res = {
        "input": dict(
            metrics_name=input_query.metrics_name,
            postgres_query=input_query.postgres_query,
        )
    }

    query_status, query_error_message = check_sql(snowflake_query)

    res["output"] = dict(
        status=query_status,
        snowflake_query=snowflake_query,
        error_message=query_error_message,
        timestamp=str(datetime.datetime.utcnow().isoformat()),
    )

    return res


def get_snowflake_query(input_query: InputQuery) -> str:
    """
    Return Snowflake "ready to execute" query
    """
    query_dict = {input_query.metrics_name: input_query.postgres_query}

    return transform(query_dict=query_dict)[input_query.metrics_name]


@router.post("/execute_snowflake", tags=["sharehouse"])
async def execute_snowflake(
    input_query: InputQuery, bearer: str = Header(None)
) -> Dict[Any, Any]:
    """
    Execute Query on Snowflake database
    """

    do_checks(input_query=input_query, bearer=bearer)

    snowflake_query = get_snowflake_query(input_query=input_query)

    result = generate_result(input_query=input_query, snowflake_query=snowflake_query)

    return result
