"""
Test file for authentication
"""
import pytest

from fastapi import HTTPException
from app.sharehouse.metrics import InputQuery


@pytest.fixture(name="create_input_query")
def test_create_input_query():
    """
    Generate InputQuery object
    """
    return InputQuery()


def test_check_mandatory_commands(create_input_query):
    """
    Check mandatory commands
    """
    create_input_query.postgres_query = "SELECT 1 FROM DUAL"

    assert create_input_query.check_mandatory_commands() is None


def test_check_mandatory_commands_exception(create_input_query):
    """
    Check mandatory commands with exception
    """
    create_input_query.postgres_query = "NO MANDATORY COMMAND"

    with pytest.raises(HTTPException):
        create_input_query.check_mandatory_commands()


def test_check_forbidden_commands(create_input_query):
    """
    Check forbidden commands
    """

    create_input_query.postgres_query = "SELECT 1 FROM TABLE"

    assert create_input_query.check_forbidden_commands() is None


def test_check_forbidden_commands_exception(create_input_query):
    """
    Check forbidden commands with exception
    """

    create_input_query.postgres_query = "DROP SOMETHING"

    with pytest.raises(HTTPException):
        create_input_query.check_forbidden_commands()
