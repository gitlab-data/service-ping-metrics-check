"""
Test file for authentication
"""
import os
from unittest import mock


import pytest

from fastapi import HTTPException
from app.sharehouse.authentication import (
    raise_http_exception,
    check_token,
    get_config,
    get_token,
    TOKEN_LENGTH,
)


@pytest.fixture(name="correct_token_mockup")
def get_correct_token_mockup():
    """
    simulate correct token
    """
    return str("a" * TOKEN_LENGTH)


@pytest.fixture(autouse=True)
def mock_settings_env_vars(correct_token_mockup):
    """
    Simulate OS env. variables
    """
    with mock.patch.dict(
        os.environ,
        {
            "SF_ROLE": "xxx",
            "SF_ACCOUNT": "xxx",
            "SF_WAREHOUSE": "xxx",
            "SF_AUTHENTICATOR": "externalbrowser",
            "SF_USERNAME": "xxx",
            "SF_PASSWORD": "xxx",
            "ACCESS_TOKEN": correct_token_mockup,
        },
    ):
        yield


def test_get_token():
    """
    Test
    """
    assert get_token(input_token=os.environ["ACCESS_TOKEN"])


def test_get_token_false():
    """
    Test
    """
    assert not get_token(input_token="456")


def test_raise_http_exception():
    """
    Test
    """
    with pytest.raises(HTTPException):
        raise_http_exception(message="scarry message")


def test_check_token():
    """
    Test
    """
    assert check_token(bearer=os.environ["ACCESS_TOKEN"]) is None


def test_check_token_short():
    """
    Test
    """
    with pytest.raises(HTTPException):
        check_token(bearer="123")


def test_check_token_wrong(correct_token_mockup):
    """
    Test
    """
    wrong_token = correct_token_mockup.replace("a", "b")

    with pytest.raises(HTTPException):
        check_token(bearer=wrong_token)


def test_get_config_externalbrowser():
    """
    Test
    """
    authenticator = os.environ["SF_AUTHENTICATOR"]

    config = get_config()

    assert authenticator == "externalbrowser"
    assert "SF_AUTHENTICATOR" in os.environ
    assert "SF_USERNAME" in os.environ
    assert "SF_PASSWORD" in os.environ

    keys = config.keys()

    assert "AUTHENTICATOR".lower() in keys
    assert "USER".lower() in keys
    assert "USERNAME".lower() not in keys
    assert "PASSWORD".lower() not in keys


def test_get_config_oauth():
    """
    Test
    """

    os.environ["SF_AUTHENTICATOR"] = o_auth = "oauth"

    authenticator = os.environ["SF_AUTHENTICATOR"]

    config = get_config()

    assert authenticator == o_auth
    assert "SF_AUTHENTICATOR" in os.environ
    assert "SF_USERNAME" in os.environ
    assert "SF_PASSWORD" in os.environ

    keys = config.keys()
    assert "AUTHENTICATOR".lower() not in keys
    assert "USER".lower() not in keys
    assert "USERNAME".lower() in keys
    assert "PASSWORD".lower() in keys
