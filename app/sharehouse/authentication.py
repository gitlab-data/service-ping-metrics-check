"""
Authentication routines
"""
from typing import Dict, Any
from fastapi import HTTPException
from decouple import config

TOKEN_LENGTH = 64


def get_token(input_token: str) -> bool:
    """
    retrieve token from .env/environment variable
    :param input_token:
    :return: bool (True/False)
    """

    secret_token = config("ACCESS_TOKEN")

    if secret_token == input_token:
        return True
    return False


def raise_http_exception(message: str) -> None:
    """
    Generic function to raise HTTP error
    """
    raise HTTPException(
        status_code=401,
        detail=message,
        headers={"WWW-Authenticate": "Bearer"},
    )


def check_token(bearer: str) -> None:
    """
    Check token validity
    :param bearer:
    :return: None
    """
    if len(bearer) != TOKEN_LENGTH:
        raise_http_exception("Token length is not valid, check is it correct.")

    if not get_token(bearer):
        raise_http_exception("Could not validate token, check is it correct.")


def get_config() -> Dict[Any, Any]:
    """
    get Snowflake configuration
    :return: Dict[Any, Any]
    """

    sf_config = dict(
        role=config("SF_ROLE"),
        account=config("SF_ACCOUNT"),
        warehouse=config("SF_WAREHOUSE"),
    )

    authenticator = config("SF_AUTHENTICATOR")

    if authenticator == "externalbrowser":
        sf_config["user"] = config("SF_USERNAME")
        sf_config["authenticator"] = authenticator
    else:
        sf_config["username"] = config("SF_USERNAME")
        sf_config["password"] = config("SF_PASSWORD")

    return sf_config
