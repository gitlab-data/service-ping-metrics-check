"""
Add testing path to be able to execute
pytest without issues both locally and in a pipeline
"""
import os
import sys

FULL_PATH = "app/sharehouse"

abs_path = os.path.dirname(os.path.realpath(__file__))
abs_path = abs_path[: abs_path.find("app")] + FULL_PATH
sys.path.append(abs_path)
